package net.marjean.railfannotebook;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@ConditionalOnProperty(name = "spring.security.oauth2.client.registration.github.clientId")
@RequiredArgsConstructor
public class CustomOAuth2Service implements OAuth2UserService<OAuth2UserRequest, OAuth2User> {

    private final UserRepository userRepository;
    private final DefaultOAuth2UserService delegate = new DefaultOAuth2UserService();

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User user = delegate.loadUser(userRequest);


        CustomOAuth2User retVal = new CustomOAuth2User(user, userRequest.getClientRegistration().getRegistrationId());

        if(log.isDebugEnabled()) {
            log.debug(user.toString());
            log.debug("Client ID: {}", userRequest.getClientRegistration().getRegistrationId());

            log.debug("**** RECEIVED OAUTH2 REQUEST ****");
            for (String attr : retVal.getAttributes().keySet()) {
                log.debug("Key: {} Value: {}", attr, retVal.getAttribute(attr));
            }
            log.debug("Name: {}", retVal.getName());
        }
        Optional<User> optionalUser = userRepository.findByUsername(retVal.getName());
        if(!optionalUser.isPresent()){

            ArrayList<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            retVal.setAuthorities(authorities);
        }
        else {
            // if we have seen this user before, read their roles from the database.
            log.debug("READING AUTHORITIES FROM DB");
            retVal.setAuthorities(Arrays.stream(optionalUser.get().getRoles().split(","))
                    .map(SimpleGrantedAuthority::new)
                      .collect(Collectors.toList())
            );
            log.debug("Authorities: {}", retVal.getAuthorities());

        }

        return retVal;
    }
}

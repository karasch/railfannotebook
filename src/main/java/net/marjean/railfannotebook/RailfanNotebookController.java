package net.marjean.railfannotebook;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.railfannotebook.datatransfer.SightingDTO;
import net.marjean.railfannotebook.model.Railroad;
import net.marjean.railfannotebook.model.Sighting;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.RailroadRepository;
import net.marjean.railfannotebook.repository.SightingRepository;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
@Controller
@RequiredArgsConstructor
public class RailfanNotebookController {

    @Value("${sighting.page.size:25}")
    private int SIGHTING_PAGE_SIZE;

    @Value("${spring.h2.console.enabled:false}")
    private Boolean h2ConsoleEnabled;

    private final RailroadRepository railroadRepository;
    private final SightingRepository sightingRepository;
    private final UserRepository userRepository;

    @Value("${environment.message:#{null}}")
    private String environmentMessage;

    /**
     * This method will be called before each request method.
     *
     * @param model The model that will be passed to Thymeleaf
     * @param principal The authenticated principal (if any)
     */
    @ModelAttribute
    public void modelAttribute(Model model, Principal principal) {
        if(principal != null) {

            Optional<User> userOptional = userRepository.findByUsername(principal.getName());
            if(userOptional.isPresent())
                model.addAttribute("currentUser", userOptional.get());
            else
                log.error("We have a user that is not present in the database.  Much confusement!");
        }
        model.addAttribute("environmentMessage", environmentMessage);
        model.addAttribute("h2ConsoleEnabled", h2ConsoleEnabled);
    }


    @GetMapping("/")
    public String index(Model model, Principal principal){
        log.debug("Loading index page");

        model.addAttribute("sightings", sightingRepository.findTop10ByOrderByIdDesc());

        return "index";
    }



    @GetMapping("/createsighting")
    public String createSighting(Model model, SightingDTO sighting, Principal principal){
        log.debug("Loading createsighting form");

        model.addAttribute("railroads", railroadRepository.findAll());

        return "createsighting";
    }

    @PostMapping("/createsighting")
    public String addSighting(Model model, SightingDTO sighting, BindingResult bindingResult, Principal principal){
        log.info("Creating sighting: {}", sighting.toString());

        if(sighting.getDirection().equalsIgnoreCase("NULL")){sighting.setDirection(null);}

        // Check required fields
        if(sighting.getLocation() == null || sighting.getLocation().isEmpty()){
            log.warn("Location not specified!");
            FieldError locationError = new FieldError("sighting", "location", "Location is a required field.");
            bindingResult.addError(locationError);
        }

        if(sighting.getSightedDate() == null || sighting.getSightedDate().isEmpty()){
            log.warn("Date not specified!");
            FieldError dateError = new FieldError("sighting", "sightedDate", "Date is a required field.");
            bindingResult.addError(dateError);
        }

        if(sighting.getSightedTime() == null || sighting.getSightedTime().isEmpty()){
            log.warn("Time not specified!");
            FieldError timeError = new FieldError("sighting", "sightedTime", "Time is a required field.");
            bindingResult.addError(timeError);
        }

        if(bindingResult.hasErrors()){
            log.info("Sighting will NOT be created due to errors!");
            model.addAttribute("railroads", railroadRepository.findAll());
            return "createsighting";
        }


        Sighting s = new Sighting();
        BeanUtils.copyProperties(sighting, s);

        if(! sighting.getRailroadId().equalsIgnoreCase("NULL")){
            Optional<Railroad> railroadOptional = railroadRepository.findById(Integer.parseInt(sighting.getRailroadId()));
            if(railroadOptional.isPresent())
                s.setRailroad(railroadOptional.get());
            else
                log.error("Someone has passed in a railroad that does not exist in the database!  Suspicious!");
        }
        Optional<User> optionalUser = userRepository.findByUsername(principal.getName());
        if(optionalUser.isPresent())
            s.setCreatedBy(optionalUser.get());
        else
            log.error("User not found in database!");
        sightingRepository.save(s);
        log.info("Sighting created successfully!");
        return "redirect:/";
    }


    //NOTE: You may see a warning here about using an Optional<> as a parameter.
    //      I think it's the right thing to do in this situation:
    //         1) It's actually an optional parameter, and we have to handle the case where it's null.
    //         2) There's only one optional parameter, so there's no confusion about which param is specified
    @SuppressFBWarnings(value = "CRLF_INJECTION_LOGS", justification = "Handled in logback config")
    @GetMapping({"/viewsightingsbyuser/{id}", "/viewsightingsbyuser/{id}/{page}"})
    public String viewSightingsByUser(Model model, @PathVariable long id, @PathVariable Optional<Integer> page, Principal principal){

        log.debug("Loading sightings for user {} page {}", id, page);

        int pg = page.orElse(1);
        Pageable pageable = PageRequest.of(pg -1, SIGHTING_PAGE_SIZE);
        Optional<User> user = userRepository.findById(id);
        if(user.isPresent()){
            Page<Sighting> sightingsPage = sightingRepository.findByCreatedBy(user.get(), pageable);
            model.addAttribute("sightings", sightingsPage);
            model.addAttribute("user", user.get());

            int totalPages = sightingsPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "listsightings";

        }
        // If we don't have that user, just redirect to index.
        return "redirect:/";
    }

    //NOTE: See previous note about Optional<> as a parameter
    @SuppressFBWarnings(value = "CRLF_INJECTION_LOGS", justification = "Handled in logback config")
    @GetMapping({"/viewsightingsbyrailroad/{id}", "/viewsightingsbyrailroad/{id}/{page}"})
    public String viewSightingsByRailroad(Model model, @PathVariable int id, @PathVariable Optional<Integer> page, Principal principal){

        log.debug("Loading sightings for RR {} page {}", id, page);

        int pg = page.orElse(1);
        Pageable pageable = PageRequest.of(pg -1, SIGHTING_PAGE_SIZE);

        Optional<Railroad> railroad = railroadRepository.findById(id);
        if(railroad.isPresent()){
            Page<Sighting> sightingPage = sightingRepository.findByRailroad(railroad.get(), pageable);
            model.addAttribute("sightings", sightingPage);
            model.addAttribute("railroad", railroad.get());

            int totalPages = sightingPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
        }
        return "listsightings";
    }

    @GetMapping("/viewrailroads")
    public String viewRailroads(Model model, Principal principal){

        log.debug("Loading viewrailroads");

        model.addAttribute("railroads", railroadRepository.findAllByOrderByName());

        return "listrailroads";
    }

    @GetMapping("/viewsighting/{id}")
    public String viewSighting(Model model, @PathVariable long id){

        log.debug("Loading sighting ID: {}", id);

        Optional<Sighting> sightingOptional = sightingRepository.findById(id);
        if(sightingOptional.isPresent())
            model.addAttribute("sighting", sightingOptional.get());
        else
            log.error("Sighting not found!");

        return "viewsighting";
    }

}

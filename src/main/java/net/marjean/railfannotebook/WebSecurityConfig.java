package net.marjean.railfannotebook;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @SuppressFBWarnings(value = {"EI_EXPOSE_REP2"}, justification = "authenticationSuccessHandler is a service.  It'll be fine.")
    private final OAuth2AuthenticationSuccessHandler authenticationSuccessHandler;

    @Value("${spring.security.oauth2.client.registration.google.clientId:#{null}}")
    private String googleId;

    @Value("${spring.security.oauth2.client.registration.github.clientId:#{null}}")
    private String githubId;

    @Bean
    @SuppressWarnings("unused") // Spring will use it.
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorize -> authorize
                        .mvcMatchers("/", "/login", "/logout", "/createaccount").permitAll()
                        .mvcMatchers("/createsighting", "/addsighting", "/editaccount", "/viewsightingsbyuser/**", "/viewsightingsbyrailroad/**", "/viewrailroads", "/viewsighting/**").hasRole("USER")
                        .mvcMatchers("/h2-console/**", "/actuator/**").hasRole("ADMIN")
                        .anyRequest().denyAll()
                )
            .formLogin().loginPage("/login")
                .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/");

        if(githubId != null || googleId != null)
            http
            .oauth2Login()
            .loginPage("/login")
            .successHandler(authenticationSuccessHandler)
            //.userInfoEndpoint()
            ;
        // It turns out that you don't actually need to specify your OIDC, OAuth2, or User Service beans
        // If they're in context, Spring will pick them up automagically.


        //https://www.logicbig.com/tutorials/spring-framework/spring-boot/jdbc-security-with-h2-console.html
        http.csrf().ignoringAntMatchers("/h2-console/**")//don't apply CSRF protection to /h2-console
            .and().headers().frameOptions().sameOrigin();//allow use of frame to same origin urls;

    }

}

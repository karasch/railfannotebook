package net.marjean.railfannotebook.datatransfer;

import lombok.Data;

import java.time.*;

/**
 * This object is used to get input from the form.
 * It's easier to get Date and Time from a form separately,
 * then combine them into a LocalDateTime object here.
 *
 */
@Data
public class SightingDTO {


    private String railroadId;
    private String trainNumber;
    private String direction;
    private String notes;
    private ZonedDateTime sightedDateTime;

    private String sightedDate;
    private String sightedTime;
    private String timezone;
    private String location;

    @SuppressWarnings("unused") //Included for completeness
    public void setSightedDate(String sightedDate){
        this.sightedDate = sightedDate;

        updateSightedDateTime();
    }


    public String getSightedDate(){ return sightedDate; }

    @SuppressWarnings("unused") // Included for completeness
    public void setSightedTime(String sightedTime){
        this.sightedTime = sightedTime;

        updateSightedDateTime();
    }

    public String getSightedTime(){ return sightedTime; }

    @SuppressWarnings("unused") // Included for completeness
    public void setTimezone(String tz){
        this.timezone = tz;
        updateSightedDateTime();
    }

    private void updateSightedDateTime() {
        if(sightedDate != null && sightedTime != null && timezone != null
                && ! sightedDate.equalsIgnoreCase("") && ! sightedTime.equalsIgnoreCase("") && ! timezone.equalsIgnoreCase("")){
            setSightedDateTime(ZonedDateTime.of(LocalDate.parse(sightedDate), LocalTime.parse(sightedTime), ZoneId.of(timezone)));
        }
    }

    /**
     * The lombok implementation of this method triggers a warning from spotbugs
     * @return a copy of the sightedDateTime
     */
    @SuppressWarnings("unused")
    public ZonedDateTime getSightedDateTime(){
        return ZonedDateTime.from(sightedDateTime);
    }

    /**
     * The lombok implementation of this method triggers a warning from spotbugs
     * @param sightedDateTime ZonedDateTime object to be copied into sightedDateTime
     */
    public void setSightedDateTime(ZonedDateTime sightedDateTime){
        this.sightedDateTime = ZonedDateTime.from(sightedDateTime);
    }

}

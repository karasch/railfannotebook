package net.marjean.railfannotebook.datatransfer;

import lombok.Data;

@Data
public class UserDTO {

    private String username;
    private String password;
    private String confirmPassword;
    private String displayName;

}

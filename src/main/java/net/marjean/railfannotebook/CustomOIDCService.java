package net.marjean.railfannotebook;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@ConditionalOnProperty(name = "spring.security.oauth2.client.registration.google.clientId")
@RequiredArgsConstructor
public class CustomOIDCService extends OidcUserService {

    private final UserRepository userRepository;

    @Override
    public OidcUser loadUser(OidcUserRequest userRequest) throws OAuth2AuthenticationException {

        OidcUser user =  super.loadUser(userRequest);
        CustomOIDCUser oidcUser = new CustomOIDCUser(user.getAuthorities(), user.getIdToken(), user.getUserInfo(), userRequest.getClientRegistration().getRegistrationId());

        if(log.isDebugEnabled()) {
            log.debug("Client ID: {}", userRequest.getClientRegistration().getRegistrationId());


            log.debug("**** RECEIVED OIDC REQUEST ****");

            for (String attr : oidcUser.getAttributes().keySet()) {
                log.debug("Key: {} Value: {}", attr, oidcUser.getAttribute(attr));
            }
            log.debug("Name: " + oidcUser.getName());
        }
        Optional<User> optionalUser = userRepository.findByUsername(oidcUser.getName());
        if(!optionalUser.isPresent()){

            ArrayList<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            oidcUser.setAuthorities(authorities);
        }
        else{
            oidcUser.setAuthorities(Arrays.stream(optionalUser.get().getRoles().split(","))
                    .map(SimpleGrantedAuthority::new)
                    .collect(Collectors.toList())
            );

            log.debug("Authorities: {}", oidcUser.getAuthorities());
        }

        return oidcUser;
    }
}

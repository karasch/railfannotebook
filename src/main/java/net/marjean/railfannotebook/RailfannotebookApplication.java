package net.marjean.railfannotebook;

import net.marjean.railfannotebook.model.Railroad;
import net.marjean.railfannotebook.model.Sighting;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.RailroadRepository;
import net.marjean.railfannotebook.repository.SightingRepository;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.SecureRandom;
import java.time.*;
import java.util.Optional;

@SpringBootApplication
public class RailfannotebookApplication {

	private static final SecureRandom random = new SecureRandom();




	public static void main(String[] args) {
		SpringApplication.run(RailfannotebookApplication.class, args);
	}

	@Bean
	@ConditionalOnProperty(name = "create.test.users")
	@Order(1)
	public Boolean createTestUsers(UserRepository userRepository, PasswordEncoder pe, @Value("${test.user.password:password}") String testUserPassword ){
		User usr = new User();
		usr.setUsername("user");
		usr.setDisplayName("Test User");
		usr.setPassword(pe.encode(testUserPassword));
		usr.setRoles("ROLE_USER,ROLE_PASSWORD");
		usr.setIsActive(true);
		userRepository.save(usr);

		User admin = new User();
		admin.setUsername("admin");
		admin.setDisplayName("Test Admin");
		admin.setPassword(pe.encode("password"));
		admin.setRoles("ROLE_USER,ROLE_ADMIN,ROLE_PASSWORD");
		admin.setIsActive(true);
		userRepository.save(admin);

		return true;
	}

	@Bean
	@ConditionalOnProperty("create.test.sightings")
	@Order(2)
	public Boolean createTestSightings(SightingRepository sightingRepository, UserRepository userRepository, RailroadRepository railroadRepository){

		Railroad cn;
		Optional<Railroad> railroadOptional = railroadRepository.findById(3);
		if(railroadOptional.isPresent())
			cn = railroadOptional.get();
		else
			throw new RuntimeException("Something has gone terribly wrong! Are railroads present in the database?");
		User defaultUser;
		Optional<User> userOptional = userRepository.findById(1L);
		if(userOptional.isPresent())
			defaultUser = userOptional.get();
		else
			throw new RuntimeException("Something has gone terribly wrong!  Are users present in the database?");

		Instant instant = Instant.now();
		ZoneId zoneId = ZoneId.of("America/Chicago");
		ZonedDateTime now = ZonedDateTime.ofInstant(instant, zoneId);

		Sighting s1 = new Sighting();
		s1.setCreatedBy(defaultUser);
		s1.setRailroad(cn);
		s1.setTrainNumber("A461");
		s1.setDirection("North");
		s1.setNotes("First Test Sighting");
		s1.setLocation("Duplainville, WI");
		s1.setSightedDateTime(now);
		sightingRepository.save(s1);

		Sighting s2 = new Sighting();
		s2.setCreatedBy(defaultUser);
		s2.setRailroad(cn);
		s2.setSightedDateTime(now);
		s2.setNotes("Second Test Sighting");
		sightingRepository.save(s2);

		return true;
	}

	@Bean
	@ConditionalOnProperty("create.random.sightings")
	@Order(3)
	public Boolean createRandomSightings(SightingRepository sightingRepository, UserRepository userRepository, RailroadRepository railroadRepository){

		//find how many users there are
		int userCount = Math.toIntExact(userRepository.count());

		//find how many railroads there are
		int railroadCount = Math.toIntExact(railroadRepository.count());


		for(int i=0; i<100; i++){

			long userId = random.nextInt(userCount)+1;
			User createdUser;
			Optional<User> userOptional= userRepository.findById(userId);
			if(userOptional.isPresent())
				createdUser = userOptional.get();
			else
				throw new RuntimeException("Something has gone terribly wrong!  Are there any users in the database?");

			int railroadId = random.nextInt(railroadCount) +1;
			Railroad rr;
			Optional<Railroad> railroadOptional = railroadRepository.findById(railroadId);
			if(railroadOptional.isPresent())
				rr = railroadOptional.get();
			else
				throw new RuntimeException("Something has gone terribly wrong!  Are there any railroads in the database?");

			int hour = random.nextInt(24);
			int minute = random.nextInt(60);
			int second = random.nextInt(60);
			ZonedDateTime time = ZonedDateTime.of(LocalDate.now(), LocalTime.of(hour, minute, second), ZoneId.of("America/Chicago"));

			Sighting s = new Sighting();
			s.setCreatedBy(createdUser);
			s.setRailroad(rr);
			s.setSightedDateTime(time);
			s.setNotes("Random sighting " + i);

			sightingRepository.save(s);

		}

		return true;
	}

}

package net.marjean.railfannotebook;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class CustomOAuth2User implements OAuth2User {

    private final OAuth2User oAuth2User;
    private Collection<GrantedAuthority> authorities;
    private final String domain;

    public CustomOAuth2User(OAuth2User user, String domain){
        oAuth2User = user;
        this.domain = domain;
    }

    public void setAuthorities(Collection<GrantedAuthority> grantedAuthorities){
        authorities = new ArrayList<>(grantedAuthorities);
    }

    @Override
    public Map<String, Object> getAttributes() {
        return oAuth2User.getAttributes();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        //return oAuth2User.getAuthorities();
        return new ArrayList<>(authorities);
    }

    @Override
    public String getName() {
        return oAuth2User.getName() + "@" + domain;
    }

}

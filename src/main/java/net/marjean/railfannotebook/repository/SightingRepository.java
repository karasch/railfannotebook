package net.marjean.railfannotebook.repository;

import net.marjean.railfannotebook.model.Railroad;
import net.marjean.railfannotebook.model.Sighting;
import net.marjean.railfannotebook.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SightingRepository extends JpaRepository<Sighting, Long> {

    Page<Sighting> findByCreatedBy(User user, Pageable pageable);
    Page<Sighting> findByRailroad(Railroad railroad, Pageable pageable);
    List<Sighting> findTop10ByOrderByIdDesc();
}

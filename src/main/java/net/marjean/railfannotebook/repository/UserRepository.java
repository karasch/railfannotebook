package net.marjean.railfannotebook.repository;

import net.marjean.railfannotebook.model.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByUsername(String username);
    Optional<User> findByDisplayName(String displayName);
}

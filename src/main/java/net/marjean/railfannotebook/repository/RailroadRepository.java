package net.marjean.railfannotebook.repository;

import net.marjean.railfannotebook.model.Railroad;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RailroadRepository extends CrudRepository<Railroad, Integer> {

    List<Railroad> findAllByOrderByName();
}

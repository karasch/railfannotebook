package net.marjean.railfannotebook;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.marjean.railfannotebook.datatransfer.UserDTO;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.PropertyResolver;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@Slf4j
@Controller
@RequiredArgsConstructor
public class UserController {

    private final static int MIN_PASSWORD_LENGTH = 8;
    private final static int MIN_USERNAME_LENGTH = 3;
    private final static int MAX_USERNAME_LENGTH = 20;

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final PropertyResolver env;

    @Value("${environment.message:#{null}}")
    private String environmentMessage;

    @Value("${account.creation.enabled:false}")
    private boolean accountCreationEnabled;

    @ModelAttribute
    public void modelAttribute(Model model){
        model.addAttribute("environmentMessage", environmentMessage);
        model.addAttribute("googleEnabled", env.containsProperty("spring.security.oauth2.client.registration.google.clientId"));
        model.addAttribute("githubEnabled", env.containsProperty("spring.security.oauth2.client.registration.github.clientId"));
    }

    @GetMapping("/createaccount")
    public String createAccount(Model model, UserDTO userDTO){
        log.debug("Loading createaccount form");

        if(accountCreationEnabled)
            return "createaccount";
        else
            return "signupclosed";
    }

    @PostMapping("/createaccount")
    public String addAccount(@Valid UserDTO userDTO, BindingResult bindingResult){
        if(accountCreationEnabled) {
            log.info("Creating account for user {}", userDTO.getUsername());

            if (userDTO.getUsername() == null || userDTO.getUsername().length() < MIN_USERNAME_LENGTH || userDTO.getUsername().length() > MAX_USERNAME_LENGTH) {
                log.warn("Username does not meet length requirements!");
                FieldError fieldError = new FieldError("userDTO", "username", userDTO.getUsername(),
                        true, null, null, "Usernames must be between " + MIN_USERNAME_LENGTH + " and " + MAX_USERNAME_LENGTH + " characters.");
                bindingResult.addError(fieldError);
            }

            if (userDTO.getPassword() == null || userDTO.getPassword().isEmpty()) {
                // Password is a required field for new accounts.  It is not required for the edit account screen.
                log.warn("Password not specified!");
                FieldError fieldError = new FieldError("userDTO", "password", "Password is a required field.");
                bindingResult.addError(fieldError);
            } else {
                FieldError passwordError = validatePassword(userDTO.getPassword(), userDTO.getConfirmPassword());
                if (passwordError != null) {
                    bindingResult.addError(passwordError);
                }
            }

            Optional<User> dupe = userRepository.findByUsername(userDTO.getUsername());
            Optional<User> dupeDisplay = userRepository.findByDisplayName((userDTO.getUsername()));
            if (dupe.isPresent() || dupeDisplay.isPresent()) {
                log.warn("Duplicate username detected!");
                FieldError error = new FieldError("userDTO", "username", "Username \"" + userDTO.getUsername() + "\" is already taken.");
                bindingResult.addError(error);
            }


            // If we have any errors, go back to the form.
            if (bindingResult.hasErrors()) {
                log.info("Account will not be created due to errors");
                return "createaccount";
            }


            User usr = new User();
            usr.setUsername(userDTO.getUsername());
            usr.setDisplayName(userDTO.getUsername());
            usr.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            usr.setRoles("ROLE_USER,ROLE_PASSWORD");
            usr.setIsActive(true);
            userRepository.save(usr);
            log.info("Account created successfully.");
            return "redirect:/login";
        }
        else {
            log.warn("POST to createaccount detected while signups are closed!");
            return "signupclosed";
        }
    }

    @GetMapping("/editaccount")
    public String editAccountForm(Model model, UserDTO userDTO, Principal principal){

        log.debug("Loading editaccount form");

        Optional<User> optionalUser = userRepository.findByUsername(principal.getName());
        if(optionalUser.isPresent()){
            User currentUser = optionalUser.get();

            model.addAttribute("currentUser", currentUser);

            userDTO.setUsername(principal.getName());
            userDTO.setDisplayName(currentUser.getDisplayName());
        }


        return "editaccount";
    }

    @PostMapping("/editaccount")
    public String editAccount(Model model, @Valid UserDTO userDTO, BindingResult bindingResult, Principal principal){

        log.info("Editing account for user {}", userDTO.getUsername());
        //NOTE: We don't care if password is null here.  If it's not provided, we just don't update it.
        FieldError passwordError = validatePassword(userDTO.getPassword(), userDTO.getConfirmPassword());
        if(passwordError != null){
            bindingResult.addError(passwordError);
        }

        //Validate the display name.
        if(userDTO.getDisplayName() != null && userDTO.getDisplayName().length() < MIN_USERNAME_LENGTH){
            log.warn("Display name not long enough!");
            //Note: FieldError(String, String, String) clears the value of the field, which is not what we want.
            //      We need the longer constructor
            FieldError fieldError = new FieldError("userDTO", "displayName", userDTO.getDisplayName(),
                    true, null, null, "Display Name must be at least " + MIN_USERNAME_LENGTH + " characters long");
            bindingResult.addError(fieldError);
        }


        Optional<User> currentUser = userRepository.findByUsername(principal.getName());
        if(!currentUser.isPresent())
            throw new RuntimeException("Trying to update a user that is not in the database?!");
        Optional<User> dupe = userRepository.findByDisplayName(userDTO.getDisplayName());
        if(dupe.isPresent() && ! dupe.get().equals(currentUser.get())){
            log.warn("Display Name already in use!");
            FieldError fieldError = new FieldError("userDTO", "displayName", userDTO.getDisplayName(),
                    true, null, null, "That display name is already in use.");
            bindingResult.addError(fieldError);
        }

        if(bindingResult.hasErrors()){
            log.info("Account will not be updated due to errors");
            return "editaccount";
        }


        User user = currentUser.get();
        if(userDTO.getPassword() != null && ! userDTO.getPassword().isEmpty())
            user.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        if(userDTO.getDisplayName() != null && ! userDTO.getDisplayName().isEmpty())
            user.setDisplayName(userDTO.getDisplayName());

        model.addAttribute("currentUser", user);
        userRepository.save(user);
        model.addAttribute("successMessage", "Account updated successfully");
        log.info("Account updated successfully.");
        return "editaccount";
    }

    @GetMapping("/login")
    public String login(Model model){
        log.debug("Loading login form");
        return "login";
    }
    // Login form with error
    @GetMapping("/login-error.html")
    public String loginError(Model model) {
        log.debug("Loading login form with error");
        model.addAttribute("loginError", true);
        return "login.html";
    }

    /**
     *
     * Validate that the passwords match, and that they are long enough
     *
     * Note: package private access for testing.
     *
     * @param password new password entered by the user
     * @param confirmPassword password confirmation entered by the user
     * @return Returns a FieldError if any is required, null if everything is ok.
     */
    FieldError validatePassword(String password, String confirmPassword) {
        if(password != null && ! password.isEmpty() && password.length() < MIN_PASSWORD_LENGTH){
            log.warn("Password not long enough!");
            return new FieldError("userDTO", "password", "Passwords must be at least " + MIN_PASSWORD_LENGTH +" characters");
        }
        if(password != null && ! password.equals(confirmPassword)){
            log.warn("Password and ConfirmPassword do not match!");
            return new FieldError("userDTO", "confirmPassword", "Passwords do not match");
        }
        return null;
    }

}

package net.marjean.railfannotebook;

import lombok.extern.slf4j.Slf4j;
import net.marjean.railfannotebook.model.User;
import net.marjean.railfannotebook.repository.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@Slf4j
@Component
public class OAuth2AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private final UserRepository userRepository;

    public OAuth2AuthenticationSuccessHandler(UserRepository userRepository){
        super();
        setUseReferer(true);
        this.userRepository = userRepository;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {


        if(authentication.getPrincipal() instanceof CustomOIDCUser){

            CustomOIDCUser oidcUser = (CustomOIDCUser) authentication.getPrincipal();
            Optional<User> optionalUser = userRepository.findByUsername(oidcUser.getName());
            if(!optionalUser.isPresent()) {
                // We have a new user
                User dbUser = new User();
                dbUser.setUsername(oidcUser.getName());
                dbUser.setRoles("ROLE_USER");
                dbUser.setDisplayName(oidcUser.getAttribute("email"));
                userRepository.save(dbUser);

                getRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, "/editaccount");

            }
            else{
                log.debug("Found a user in the database!");
                // in a perfect world, this would redirect to the page the user clicked on to trigger the login,
                // but I can't seem to figure that out.
                super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
            }

        }

        else if(authentication.getPrincipal() instanceof CustomOAuth2User){
            CustomOAuth2User oAuth2User = (CustomOAuth2User) authentication.getPrincipal();
            Optional<User> optionalUser = userRepository.findByUsername(oAuth2User.getName());
            if(!optionalUser.isPresent()){
                // We have a new user
                User dbUser = new User();
                dbUser.setUsername(oAuth2User.getName());
                dbUser.setRoles("ROLE_USER");
                dbUser.setDisplayName(oAuth2User.getAttribute("login"));
                userRepository.save(dbUser);

                getRedirectStrategy().sendRedirect(httpServletRequest, httpServletResponse, "/editaccount");
            }
            else{
                // in a perfect world, this would redirect to the page the user clicked on to trigger the login,
                // but I can't seem to figure that out.
                super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
            }
        }
        else{
            log.warn("Not an OIDC or OAuth2 user!");
        }
    }
}

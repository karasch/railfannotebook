package net.marjean.railfannotebook;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;

import java.util.ArrayList;
import java.util.Collection;

public class CustomOIDCUser extends DefaultOidcUser {
    private final String domain;
    private Collection<? extends GrantedAuthority> authorities;

    public CustomOIDCUser(Collection<? extends GrantedAuthority> authorities, OidcIdToken idToken, OidcUserInfo userInfo, String domain) {
        super(authorities, idToken, userInfo);
        this.authorities = new ArrayList<>(authorities);
        this.domain = domain;
    }

    @Override
    public String getName(){
        return super.getName() + "@" + domain;
    }

    public void setAuthorities(Collection<GrantedAuthority> authorities){
        this.authorities = new ArrayList<>(authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){
        return new ArrayList<>(authorities);
    }

    /**
     * I doubt this will ever be used, but spotbugs complains if it's missing.
     * @param other the other object to compare
     * @return if the objects are equal
     */
    @Override
    public boolean equals(Object other){
        if(!(other instanceof CustomOIDCUser)){
            return false;
        }

        return (getName().equals(((CustomOIDCUser) other).getName()) && getAuthorities().equals(((CustomOIDCUser) other).getAuthorities()));
    }

    /**
     * Naive hashCode implementation, as requested by spotbugs
     * @return a hashcode for the object
     */
    @Override
    public int hashCode(){
        return getName().hashCode() + getAuthorities().hashCode();
    }
}

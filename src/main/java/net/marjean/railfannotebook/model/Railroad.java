package net.marjean.railfannotebook.model;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Railroad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String reportingMark;

    @Override
    @SuppressFBWarnings(value = "BC_EQUALS_METHOD_SHOULD_WORK_FOR_ALL_OBJECTS", justification = "It does check the type, but spotbugs can't find it.")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Railroad railroad = (Railroad) o;

        return Objects.equals(id, railroad.id);
    }

    @Override
    public int hashCode() {
        return 1107241746;
    }
}

package net.marjean.railfannotebook.model;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.Objects;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@SuppressFBWarnings(value = {"EI_EXPOSE_REP", "EI_EXPOSE_REP2"}, justification = "Complaints about generated code.")
// NOTE: lombok has no plans to fix the way they generate getters and setters. https://github.com/projectlombok/lombok/issues/420
public class Sighting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToOne
    private Railroad railroad;
    //private String railroad;
    private String trainNumber;
    private String direction;
    private String notes;
    @Column(columnDefinition = "TIMESTAMP")
    private ZonedDateTime sightedDateTime;
    private String location;
    @OneToOne
    private User createdBy;

    @Override
    @SuppressFBWarnings(value = "BC_EQUALS_METHOD_SHOULD_WORK_FOR_ALL_OBJECTS", justification = "It does check the type, but spotbugs can't find it.")
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Sighting sighting = (Sighting) o;

        return Objects.equals(id, sighting.id);
    }

    @Override
    public int hashCode() {
        return 550167302;
    }
}

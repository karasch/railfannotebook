insert into railroad (name, reporting_mark) values ('BNSF Railroad', 'BNSF');
insert into railroad (name, reporting_mark) values ('Canadian Pacific', 'CP');
insert into railroad (name, reporting_mark) values ('Canadian National', 'CN');
insert into railroad (name, reporting_mark) values ('Union Pacific', 'UP');
insert into railroad (name, reporting_mark) values ('Kansas City Southern', 'KCS');
insert into railroad (name, reporting_mark) values ('Norfolk Southern', 'NS');
insert into railroad (name, reporting_mark) values ('CSX Transportation', 'CSXT');
insert into spring_user (id, display_name, username) values (1, 'Default User', 'default_user');

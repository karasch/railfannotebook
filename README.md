# Railfan Notebook

An application for people who enjoy watching trains.  Track when and where you have seen trains and see where others have done the same.

A test instance of this application has been deployed to https://railfannotebook-test.herokuapp.com/
<br/>I am currently working on a few more improvements before deploying this application to a permanent environment.

## Caveat

Note: This is very much a work in progress.  I am building this to demonstrate my skills and learn some new ones more than I expect this to see any actual use.

## Testing toggles
There are some properties that will enable features for testing:

* `environment.message` Adds a warning to all screens with the value of this property
* `create.test.users` Creates a normal password user and an admin user on startup
* `test.user.password` Set the password for test users if any.  Defaults to something very weak.
* `create.test.sightings` Creates a couple sightings assigned to Default User
* `create.random.sightings` Creates 100 sightings assigned to random users and railroads

## Other properties
* `sighting.page.size` controls the number of sightings shown in the list views (defaults to 25)
* `account.creation.enabled` Enables/disables new user account creation (defaults to false)

## Features to implement
* List sightings by time reported 
* Display times in local time zone
* Geolocate cities and store Lat/Long 
* Admins should be able to add/edit railroads 
* Users should be able to edit their sightings
* Add an "other" option to the railroad list, and a sort by column
* Add locomotives to Sightings
* Add a link to images field to Sightings

## The other stuff TODO list
* Refactor all the Spring security stuff into its own package
* Investigate Bootstrap's pagination feature
* Do something about account creation via OAuth2/OIDC when local account creation is disabled

